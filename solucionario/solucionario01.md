# Ejercicios propuestos

## Ejercicio 01
Necesita almacenar los datos de amigos en una tabla.
Los datos que guardará serán: apellido, nombre, domicilio y teléfono.


### Elimine la tabla "agenda" Si no existe, un mensaje indicará tal situación.


### Intente crear una tabla llamada "*agenda"

```sql

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

``` 
##### salida
```sql

[2023-06-01 13:56:36] [42000][903] ORA-00903: nombre de tabla no válido

```
##### correcto
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```

Aparece un mensaje de error indicando que usamos un caracter inválido ("*") para el nombre de la tabla.


### Cree una tabla llamada "agenda", debe tener los siguientes campos: apellido, varchar2(30); nombre, varchar2(20); domicilio, varchar2 (30) y telefono, varchar2(11)

##### solucion
```sql
MARCO> CREATE TABLE agenda (
           apellido VARCHAR2(30),
           nombre VARCHAR2(20),
           domicilio VARCHAR2(30),
           telefono VARCHAR2(11)
       )
[2023-06-01 14:04:13] [42000][955] ORA-00955: este nombre ya lo está utilizando otro objeto existente

```

Un mensaje indica que la tabla ha sido creada exitosamente.

### Intente crearla nuevamente.Aparece mensaje de error indicando que el nombre ya lo tiene otro objeto.
```sql
MARCO> CREATE TABLE agenda1 (
           apellido VARCHAR2(30),
           nombre VARCHAR2(20),
           domicilio VARCHAR2(30),
           telefono VARCHAR2(11)
       )

[2023-06-01 14:04:21] completed in 28 ms
```

### Visualice las tablas existentes (all_tables)La tabla "agenda" aparece en la lista.
```sql
68 AGENDA1

```

### Visualice la estructura de la tabla "agenda" (describe) Aparece la siguiente tabla:

```sql

Name Null Type
-----------------------

APELLIDO VARCHAR2(30)
NOMBRE  VARCHAR2(20)
DOMICILIO VARCHAR2(30)
TELEFONO VARCHAR2(11)

```

## Ejercicio 02
Necesita almacenar información referente a los libros de su biblioteca personal. Los datos que guardará serán: título del libro, nombre del autor y nombre de la editorial.


##### Elimine la tabla "libros" Si no existe, un mensaje indica tal situación.
```sql
[42000][942] ORA-00942: la tabla o vista no existe
```

##### Verifique que la tabla "libros" no existe (all_tables) No aparece en la lista.
```sql
TABLE_NAME
-
-
-

```

##### Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo, varchar2(20); autor, varchar2(30) y editorial, varchar2(15)
```sql
MARCO> CREATE TABLE libros (
           titulo VARCHAR2(20),
           autor VARCHAR2(30),
           editorial VARCHAR2(15)
       )
[2023-06-01 14:20:14] completed in 9 ms

```

##### Intente crearla nuevamente: Aparece mensaje de error indicando que existe un objeto con el nombre "libros".
```sql
MARCO> CREATE TABLE libros (
                  titulo VARCHAR2(20),
                  autor VARCHAR2(30),
                  editorial VARCHAR2(15)
              )
[2023-06-01 14:21:03] [42000][955] ORA-00955: este nombre ya lo está utilizando otro objeto existente
[2023-06-01 14:21:03] Position: 13

```

##### Visualice las tablas existentes
```sql

DUAL
SYSTEM_PRIVILEGE_MAP
TABLE_PRIVILEGE_MAP
USER_PRIVILEGE_MAP
STMT_AUDIT_OPTION_MAP
AV_DUAL
AUDIT_ACTIONS
ALL_UNIFIED_AUDIT_ACTIONS
WRR$_REPLAY_CALL_FILTER
SCHEDULER_FILEWATCHER_QT
XDB$IMPORT_TT_INFO
XDB$IMPORT_QN_INFO
XDB$IMPORT_NM_INFO
XDB$IMPORT_PT_INFO
XDB_INDEX_DDL_CACHE
HELP
DR$OBJECT_ATTRIBUTE
DR$POLICY_TAB
DR$THS
DR$THS_PHRASE
DR$NUMBER_SEQUENCE
AW$EXPRESS
AW$AWMD
AW$AWCREATE
AW$AWCREATE10G
AW$AWXML
AW$AWREPORT
SDO_TIN_PC_SEQ
SDO_TIN_PC_SYSDATA_TABLE
SRSNAMESPACE_TABLE
SDO_UNITS_OF_MEASURE
SDO_PRIME_MERIDIANS
SDO_ELLIPSOIDS
SDO_DATUMS
SDO_COORD_SYS
SDO_COORD_AXIS_NAMES
SDO_COORD_AXES
SDO_COORD_REF_SYS
SDO_COORD_OP_METHODS
SDO_COORD_OPS
SDO_PREFERRED_OPS_SYSTEM
SDO_PREFERRED_OPS_USER
SDO_COORD_OP_PATHS
SDO_COORD_OP_PARAMS
SDO_COORD_OP_PARAM_USE
SDO_COORD_OP_PARAM_VALS
SDO_CS_SRS
NTV2_XML_DATA
SDO_SRIDS_BY_URN
SDO_SRIDS_BY_URN_PATTERN
SDO_CRS_GEOGRAPHIC_PLUS_HEIGHT
SDO_PROJECTIONS_OLD_SNAPSHOT
SDO_ELLIPSOIDS_OLD_SNAPSHOT
SDO_DATUMS_OLD_SNAPSHOT
SDO_INDEX_HISTOGRAM_TABLE
SDO_DIST_METADATA_TABLE
SDO_XML_SCHEMAS
SDO_FEATURE_USAGE
SDO_WS_CONFERENCE
SDO_WS_CONFERENCE_RESULTS
SDO_WS_CONFERENCE_PARTICIPANTS
SDO_XSD_TABLE
OGIS_SPATIAL_REFERENCE_SYSTEMS
OGIS_GEOMETRY_COLUMNS
SDO_GEOR_XMLSCHEMA_TABLE
SDO_GEOR_PLUGIN_REGISTRY
AGENDA
AGENDA1
LIBROS
FINALHIST$
MODELGTTRAW$
WRI$_ADV_ASA_RECO_DATA
WRI$_HEATMAP_TOPN_DEP1
WRI$_HEATMAP_TOPN_DEP2
PLAN_TABLE$
OL$
OL$HINTS
OL$NODES
KU$_LIST_FILTER_TEMP
KU$_LIST_FILTER_TEMP_2
DATA_PUMP_XPL_TABLE$
KU$NOEXP_TAB
KU$_SHARD_DOMIDX_NAMEMAP
KU$XKTFBUE
ODCI_SECOBJ$
ODCI_WARNINGS$
ODCI_PMO_ROWIDS$
XS$VALIDATION_TABLE
IMPDP_STATS
KU$_DATAPUMP_MASTER_12_2
KU$_DATAPUMP_MASTER_12_0
KU$_DATAPUMP_MASTER_11_2
KU$_DATAPUMP_MASTER_11_1_0_7
KU$_DATAPUMP_MASTER_11_1
KU$_DATAPUMP_MASTER_10_1
SPD_SCRATCH_TAB
XDB$XIDX_IMP_T
SAM_SPARSITY_ADVICE
SDO_CS_CONTEXT_INFORMATION
SDO_ST_TOLERANCE
SDO_TTS_METADATA_TABLE
SDO_TXN_JOURNAL_GTT
SDO_TXN_JOURNAL_REG
SDO_TXN_IDX_EXP_UPD_RGN
SDO_TOPO_TRANSACT_DATA
SDO_TOPO_RELATION_DATA
SDO_TOPO_DATA$
SDO_WFS_LOCAL_TXNS
SDO_GEOR_DDL__TABLE$$
SDO_GR_MOSAIC_0
SDO_GR_MOSAIC_1
SDO_GR_MOSAIC_2
SDO_GR_MOSAIC_3
SDO_GR_MOSAIC_CB
SDO_GR_PARALLEL
SDO_GR_RDT_1
RDF_PARAMETER


```

##### Visualice la estructura de la tabla "libros": Aparece "libros" en la lista.
```sql
MARCO> DESCRIBE libros;
[2023-06-01 14:22:06] [42000][900] ORA-00900: sentencia SQL no válida

```

##### Elimine la tabla
```sql
MARCO> DROP TABLE libros
[2023-06-01 14:22:56] completed in 152 ms

```

##### Intente eliminar la tabla. Un mensaje indica que no existe.
```sql
MARCO> DROP TABLE libros
[2023-06-01 14:23:09] [42000][942] ORA-00942: la tabla o vista no existe
```