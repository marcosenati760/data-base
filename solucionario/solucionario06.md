Ejercicios propuesto

Ejercicio 01
Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.


##### Elimine "articulos"
```sql
drop table articulos 
--salida

Table ARTICULOS borrado.


```


##### Cree la tabla, con la siguiente estructura:


```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(6,2),
    cantidad number(3)
);
--salida
Table ARTICULOS creado.
```



#### Vea la estructura de la tabla.
```sql
describe articulos 
--salida
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(5)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(6,2)  
CANTIDAD           NUMBER(3) 
```


#### Ingrese algunos registros:


```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);
--salida 
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```




##### Seleccione los datos de las impresoras (2 registros)
```sql
select *from articulos where nombre ='impresora'
--salida
1	impresora	Epson Stylus C45	400,8	20
2	impresora	Epson Stylus C85	500	30
```


#### Seleccione los artículos cuyo precio sea mayor o igual a 400 (3 registros)
```sql
select * from articulos where precio >= '400'
--salida 
1	impresora	Epson Stylus C45	400,8	20
2	impresora	Epson Stylus C85	500	30
3	monitor	Samsung 14	800	10
```


#### Seleccione el código y nombre de los artículos cuya cantidad sea menor a 30 (2 registros)
```sql
select codigo,nombre from articulos where cantidad <= '30'
--salida
1	impresora
2	impresora
3	monitor

```


#### Selecciones el nombre y descripción de los artículos que NO cuesten $100 (4 registros)
```sql
select nombre,descripcion  from articulos where precio <> '100'
--salida
impresora	Epson Stylus C45
impresora	Epson Stylus C85
monitor	Samsung 14
teclado	español Biswal

```



# Ejercicio 02
Un video club que alquila películas en video almacena la información de sus películas en alquiler en una tabla denominada "peliculas".

#### Elimine la tabla.

```sql
drop table peliculas;
--salida

Table PELICULAS borrado.

```


#### Cree la tabla eligiendo el tipo de dato adecuado para cada campo:

```sql
create table peliculas(
    titulo varchar2(20),
    actor varchar2(20),
    duracion number(3),
    cantidad number(1)
);
--salida
Table PELICULAS creado.
```



#### Ingrese los siguientes registros:

```sql
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,4);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,1);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',80,2);
--salida
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```




#### Seleccione las películas cuya duración no supere los 90 minutos (2 registros)
```sql
select *from peliculas where duracion <= '90'
--salida
Mujer bonita	Julia R.	90	1
Elsa y Fred	China Zorrilla	80	2
```


#### Seleccione el título de todas las películas en las que el actor NO sea "Tom Cruise" (2 registros)
```sql 
select titulo from peliculas where actor <>'Tom Cruise'
--salida
Mujer bonita
Elsa y Fred
```

#### Muestre todos los campos, excepto "duracion", de todas las películas de las que haya más de 2 copias (2 registros)
```sql
select titulo,actor,cantidad from peliculas where cantidad > '2'
--salida
Mision imposible	Tom Cruise	3
Mision imposible 2	Tom Cruise	4
```