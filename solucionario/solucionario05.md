# Ejercicios propuestos

## Ejercicio 01
Trabaje con la tabla "agenda" en la que registra los datos de sus amigos.


#### Elimine "agenda"
```sql 

drop table agenda
Informe de error -
ORA-00942: la tabla o vista no existe

``` 

#### Cree la tabla, con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):


```sql

create table agenda(
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    telefono varchar2(11)
);
--salida

Table AGENDA creado.
```


#### Visualice la estructura de la tabla "agenda" (4 campos)
```sql 
describe agenda;
--salida
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(30) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 
```

#### Ingrese los siguientes registros ("insert into"):

```sql 


insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454');
--salida
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```




#### Seleccione todos los registros de la tabla (5 registros)
```sql 
--salida 
Acosta	    Ana	    Colon      123 4234567

Bustamante	Betina	Avellaneda 135 4458787

Lopez	    Hector	Salta      545 4887788

Lopez	    Luis	Urquiza    333 4545454
```


#### Seleccione el registro cuyo nombre sea "Marisa" (1 registro)

```sql 
select * from agenda WHERE NOMBRE= 'Marisa'
--salida
Lopez	Marisa	Urquiza 333	4545454
```

#### Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)
```sql 
select NOMBRE, DOMICILIO from agenda WHERE APELLIDO= 'Lopez'

--salida
Hector,Salta 545
Luis,Urquiza 333
Marisa,Urquiza 333
```



#### Seleccione los nombres y domicilios de quienes tengan apellido igual a "lopez" (en minúsculas)
```sql 
select NOMBRE, DOMICILIO from agenda WHERE APELLIDO= 'lopez'

--salida
no result
```
No aparece ningún registro, ya que la cadena "Lopez" no es igual a la cadena "lopez".


#### Muestre el nombre de quienes tengan el teléfono "4545454" (2 registros)
```sql 
select NOMBRE,TELEFONO from agenda WHERE TELEFONO= '4545454'



--salida
Luis	4545454
Marisa	4545454
```


# Ejercicio 02
Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla llamada "articulos".


#### Elimine la tabla si existe.
```SQL
DROP TABLE ARTICULOS 
--SALIDA
Table ARTICULOS borrado.

```


#### Cree la tabla "articulos" con la siguiente estructura:


```SQL
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2)
);
--SALIDA
Table ARTICULOS creado.
```


#### Vea la estructura de la tabla:

```SQL
 describe articulos;
 --SALIDA 
 Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(5)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(7,2) 
```


#### Ingrese algunos registros:
```SQL

insert into articulos (codigo, nombre, descripcion, precio) values (1,'impresora','Epson Stylus C45',400.80);
insert into articulos (codigo, nombre, descripcion, precio) values (2,'impresora','Epson Stylus C85',500);
insert into articulos (codigo, nombre, descripcion, precio) values (3,'monitor','Samsung 14',800);
insert into articulos (codigo, nombre, descripcion, precio) values (4,'teclado','ingles Biswal',100);
insert into articulos (codigo, nombre, descripcion, precio) values (5,'teclado','español Biswal',90);
--SALIDA
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```




#### Seleccione todos los datos de los registros cuyo nombre sea "impresora" (2 registros)
```sql
SELECT *FROM ARTICULOS WHERE nombre ='impresora'
--salida
1	impresora	Epson Stylus C45	400,8
2	impresora	Epson Stylus C85	500

```

#### Muestre sólo el código, descripción y precio de los teclados (2 registros)
```SQL 

select DESCRIPCION, PRECIO FROM ARTICULOS WHERE NOMBRE ='teclado'
--SALIDA
ingles Biswal	100
español Biswal	90
```