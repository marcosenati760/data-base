# 3. Tipos de datos

## Ejercicios propuestos

### Ejercicio 01
Un videoclub que alquila películas en video almacena la información de sus películas en una tabla llamada "peliculas"; para cada película necesita los siguientes datos:

 -nombre, cadena de caracteres de 20 de longitud,
 -actor, cadena de caracteres de 20 de longitud,
 -duración, valor numérico entero que no supera los 3 dígitos.
 -cantidad de copias: valor entero de un sólo dígito (no tienen más de 9 copias de cada película).




##### Elimine la tabla "peliculas" si ya existe.
```sql
DROP TABLE peliculas;

--salida
Error que empieza en la línea: 1 del comando :
drop table peliculas
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```


##### Cree la tabla eligiendo el tipo de dato adecuado para cada campo.



Note que los campos "duracion" y "cantidad", que almacenarán valores sin decimales, fueron definidos de maneras diferentes, en el primero especificamos el valor 0 como cantidad de decimales, en el segundo no especificamos cantidad de decimales, es decir, por defecto, asume el valor 0.

```sql
CREATE TABLE peliculas (
    nombre VARCHAR2(20),
    actor VARCHAR2(20),
    duracion NUMBER(3, 0),
    cantidad NUMBER(1)
);
--salida

Table PELICULAS creado.


```
##### Vea la estructura de la tabla.

```sql
DESCRIBE poeliculas;

--salida

Nombre   ¿Nulo? Tipo         
-------- ------ ------------ 
NOMBRE          VARCHAR2(20) 
ACTOR           VARCHAR2(20) 
DURACION        NUMBER(3)    
CANTIDAD        NUMBER(1)


```

##### Ingrese los siguientes registros:
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);

--salida
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

##### Muestre todos los registros (4 registros)
```sql
select * from peliculas;

--salida
NOMBRE, ACTOR, DURACION, CANTIDAD
Mision imposible	Tom Cruise	128	3
Mision imposible 2	Tom Cruise	130	2
Mujer bonita	Julia Roberts	118	3
Elsa y Fred	China Zorrilla	110	2

```
##### Intente ingresar una película con valor de cantidad fuera del rango permitido:
```sql
 insert into peliculas (nombre, actor, duracion, cantidad)
  values ('Mujer bonita','Richard Gere',1200,10);
  --salida
  Error que empieza en la línea: 22 del comando -
insert into peliculas (nombre, actor, duracion, cantidad)
  values ('Mujer bonita','Richard Gere',1200,10)
Error en la línea de comandos : 23 Columna : 41
Informe de error -
Error SQL: ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
01438. 00000 -  "value larger than specified precision allowed for this column"
*Cause:    When inserting or updating records, a numeric value was entered
           that exceeded the precision defined for the column.
*Action:   Enter a value that complies with the numeric column's precision,
           or use the MODIFY option with the ALTER TABLE command to expand
           the precision.
```
##### Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',120.20,4);
--salida 
1 fila insertadas.
```



##### Muestre todos los registros para ver cómo se almacenó el último registro ingresado.
```sql
select * from peliculas;
--salida
NOMBRE, ACTOR, DURACION, CANTIDAD
Mision imposible	Tom Cruise	128	3
Mision imposible 2	Tom Cruise	130	2
Mujer bonita	Julia Roberts	118	3
Elsa y Fred	China Zorrilla	110	2
Mujer bonita	Richard Gere	120	4
Mujer bonita	Richard Gere	120	4

```

##### Intente ingresar un nombre de película que supere los 20 caracteres.

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('el destructr de galaxias','Richard Gere',120.20,4);
--salida
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "MARCO"."PELICULAS"."NOMBRE" (real: 25, máximo: 20)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"

```

# Ejercicio 02
Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.


#####  Elimine la tabla si existe.
```sql
drop table empleados;
--salida
Error que empieza en la línea: 34 del comando :
drop table empleados
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```


##### Cree la tabla eligiendo el tipo de dato adecuado para cada campo:

```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1);
    domicilio varchar2(30),
    sueldobasico number(6,2)
);
--salida
Table EMPLEADOS creado.
```



##### Verifique que la tabla existe consultando
```sql
SELECT table_name
FROM all_tables;
--salida
TABLE_NAME
70 EMPLEADOS


```
##### Vea la estructura de la tabla (5 campos)
```sql
select * from empleados,

```

##### Ingrese algunos registros:

```SQL

insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);

--salida
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```

##### Seleccione todos los registros (3 registros)
```sql
select * from empleados;
--salida
Juan Perez	22333444	m	Sarmiento 123	500
Ana Acosta	24555666	f	Colon 134	650
Bartolome Barrios	27888999	m	Urquiza 479	800

```
##### Intente ingresar un registro con el valor "masculino" en el campo "sexo".
Un mensaje indica que el campo está definido para almacenar 1 solo caracter como máximo y está intentando ingresar 9 caracteres.


##### Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico"
```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','masculino','Urquiza 479',800);

--salida
Error SQL: ORA-12899: el valor es demasiado grande para la columna "MARCO"."EMPLEADOS"."SEXO" (real: 9, máximo: 1)
```

##### Elimine la tabla
```sql
drop table empleados;
--salida 
Table EMPLEADOS borrado.
```