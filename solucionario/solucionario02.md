# 2. Ingresar registros (insert into- select)

## Ejercicios propuestos

### Ejercicio 01
Trabaje con la tabla "agenda" que almacena información de sus amigos.


##### Elimine la tabla "agenda"
```sql
drop table agenda

Table AGENDA borrado.

```


##### Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)

```sql

 CREATE TABLE agenda (
    apellido VARCHAR2(30),
    nombre VARCHAR2(20),
    domicilio VARCHAR2(30),
    telefono VARCHAR2(11)
);
--salida..

Table AGENDA creado.

```
##### Visualice las tablas existentes para verificar la creación de "agenda" (all_tables)
```sql
SELECT table_name
FROM all_tables;

--salida..

TABLE_NAME
68 AGENDA
```


#####  Visualice la estructura de la tabla "agenda" (describe)

```sql
describe agenda;

--salida..

Table AGENDA creado.

Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11)

```
##### Ingrese los siguientes registros:


```sql
insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');

--salida..

1 fila insertadas.


1 fila insertadas.
```
##### Seleccione todos los registros de la tabla.
```sql
SELECT * FROM agenda;

--salida..

APELLIDO NOMBRE DOMICILIO TELEFONO
Moreno	Alberto	Colon 123	4234567
Torres	Juan	Avellaneda 135	4458787
```

##### Elimine la tabla "agenda"
```sql
DROP TABLE agenda;
--salida..

Table AGENDA borrado.

```

##### Intente eliminar la tabla nuevamente (aparece un mensaje de error)

```sql
DROP TABLE agenda;
--salida..

Error que empieza en la línea: 25 del comando :
DROP TABLE agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```

## Ejercicio 02
Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.


##### Elimine la tabla "libros"
```sql
DROP TABLE libros;

--salida

Error que empieza en la línea: 2 del comando :
DROP TABLE libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```

##### Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15)
```sql
CREATE TABLE libros (
    titulo VARCHAR2(20),
    autor VARCHAR2(30),
    editorial VARCHAR2(15)
);

--salida
Table LIBROS creado.

```

##### Visualice las tablas existentes.
```sql
SELECT table_name
FROM all_tables;

--salida
TABLE_NAME
68 LIBROS

```

##### Visualice la estructura de la tabla "libros"
```sql
DESCRIBE libros;

--salida

 Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 


```

##### Muestra los campos y los tipos de datos de la tabla "libros".
```sql
SELECT column_name, data_type
FROM all_tab_columns
WHERE table_name = 'LIBROS';

--salida
COLUMN_NAME, DATA_TYPE
TITULO	VARCHAR2
AUTOR	VARCHAR2
EDITORIAL	VARCHAR2

```
##### Ingrese los siguientes registros:
 ```sql
 INSERT INTO libros (titulo, autor, editorial) VALUES ('Libro 1', 'Autor 1', 'Editorial 1');
INSERT INTO libros (titulo, autor, editorial) VALUES ('Libro 2', 'Autor 2', 'Editorial 2');

--salida
1 fila insertadas.


1 fila insertadas.


```
##### Muestre todos los registros (select) de "libros"
```sql
SELECT * FROM libros;

--salida
TITULO, AUTOR, EDITORIAL
Libro 1	Autor 1	Editorial 1
Libro 2	Autor 2	Editorial 2

```